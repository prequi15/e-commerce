export default function AdminPageTitle({ title, button }) {
    return (
        <div className="d-flex flex-column flex-md-row align-items-center justify-content-between mb-3">
            <h2 className="mb-0">{ title }</h2>
            { button ? button : "" }
        </div>
    )
}