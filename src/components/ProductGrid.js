import { Link } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";

export default function ProductGrid({ products }) {
    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
      });

    return (
        <>
            <Row>
                {
                    products.length > 0 ? 
                        products.map(p => 
                            <Col key={p._id} sm={6} md={4} lg={3}>
                                <Link to={`/product/${p.slug}`} className="d-block shadow-sm mb-3 text-decoration-none">                                    
                                    <Card className="rounded-0 product-item">
                                        <Card.Img variant="top" src={p.photoUrl === "" ? "/no-product-image.jpg" : p.photoUrl} className="rounded-0" />
                                        <Card.Body>
                                            <Card.Title className="text-nowrap text-dark text-ellipsis overflow-hidden fs-6">{p.name}</Card.Title>
                                            <div className="fs-5 fw-bold">{p.discount > 0 ? numberFormat.format(p.discountedPrice) : numberFormat.format(p.price)}</div>
                                            {
                                                p.discount > 0 ?
                                                <small className="text-dark"><span className="text-muted text-decoration-line-through">{numberFormat.format(p.price)}</span> - {p.discount}%</small> : <small>&nbsp;</small>
                                            }
                                        </Card.Body>
                                    </Card>
                                </Link>
                            </Col>
                        )
                        : ""
                }
            </Row>
        </>
    )
}