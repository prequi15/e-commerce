import { useContext, useState } from "react";
import CategoryContext from "../CategoryContext";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Alert from 'react-bootstrap/Alert';

export default function CategoryDelete() {
    const { categories, setCategories, selCategory, setSelCategory, showDeleteModal, setShowDeleteModal } = useContext(CategoryContext);

    const [showWarning, setShowWarning] = useState(false);

    let name = "";

    if (selCategory !== null) {
        name = selCategory.name;
    }

    async function deleteCategory() {
        const { _id } = selCategory;
        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories/${_id}/archive`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            }
        });

        const result = await res.json();

        if (typeof result.errorCode !== "undefined") {
            setShowWarning(true);
        }
        else if(result) {
            setCategories(categories.map(c => c._id === _id ? { ...c, isDeleted: true } : c));
            setShowDeleteModal(false);
            setSelCategory(null);
            setShowWarning(false);
        }
    }

    function cancelDelete() {
        setShowDeleteModal(false);
        setShowWarning(false);
    }

    return (
        <Modal show={showDeleteModal} onHide={cancelDelete} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Delete Category</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {
                    showWarning ?
                        <Alert variant="warning" className="rounded-0">
                            You can't delete a category with products!
                        </Alert> : ""
                }
                Are you sure you want to delete <strong>{name}</strong> category?
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-between">
                <Button variant="danger" className="rounded-0" onClick={deleteCategory}>
                    Delete
                </Button>
                <Button variant="secondary" className="rounded-0" onClick={cancelDelete}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}