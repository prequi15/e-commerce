import { useState, useEffect } from "react";

export default function OrderProductItem({ productId, price, quantity, subtotal }) {
    const [product, setProduct] = useState(null);

    async function getProduct() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`);
        const data = await res.json();
        if (data) {
            setProduct(data);
        }
    }

    useEffect(()=> {
        getProduct();
    }, [productId]);

    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    return (
        <>
            {
                product !== null ? 
                <tr>
                    <td className="align-middle">
                        <div className="overflow-hidden" style={{ width: "50px", height: "50px" }}>
                            <img className="img-fluid" src={product.photoUrl === "" ? "/no-product-image.jpg" : product.photoUrl} alt={product.name} />
                        </div>
                    </td>
                    <td className="align-middle">{product.name}</td>
                    <td className="align-middle">{numberFormat.format(price)}</td>
                    <td className="align-middle">{quantity}</td>
                    <td className="align-middle">{numberFormat.format(subtotal)}</td>
                </tr> : ""
            }
        </>
    )
}