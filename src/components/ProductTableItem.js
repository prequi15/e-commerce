import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import ProductContext from "../ProductContext";
import Button from "react-bootstrap/esm/Button";
import Form from "react-bootstrap/esm/Form";
import * as Icon from "react-feather";
import { format } from "date-fns";

export default function ProductTableItem({ item }) {
    const navigate = useNavigate();

    const { photoUrl, name, categoryId, price, discount, stocks, isActive, createdOn } = item;

    const { categories, setSelProduct, setShowUpdStatusModal, setShowUpdateForm, setShowDeleteModal } = useContext(ProductContext);  

    function continueUpdateStatus() {
        setSelProduct(item);
        setShowUpdStatusModal(true);
    }

    function continueUpdateProduct() {
        setSelProduct(item);
        setShowUpdateForm(true);
        navigate(`/products/${item._id}`);
    }

    function continueDelete() {
        setSelProduct(item);
        setShowDeleteModal(true);
    }

    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    return (
        <tr>
            <td className="align-middle border-top">
                <div className="border d-flex align-items-center" style={{ width: "50px", height: "50px" }}>
                    <img className="img-fluid" src={photoUrl === "" ? "/no-product-image.jpg" : photoUrl} alt="Product Thumbnail" />
                </div>
            </td>
            <td className="align-middle border-top">{name}</td>
            <td className="align-middle border-top">{categories.length > 0 ? categories.find(c => c._id === categoryId).name : ""}</td>
            <td className="align-middle border-top">{numberFormat.format(price)}</td>
            <td className="align-middle border-top">{discount > 0 ? `${discount}% OFF` : "0%"}</td>
            <td className="align-middle border-top">{stocks}</td>
            <td className="align-middle border-top">
                <Form.Check 
                    type="switch"
                    id="custom-switch-1"
                    label=""
                    onChange={continueUpdateStatus}
                    checked={isActive}
                />
            </td>
            <td className="align-middle border-top">{format(new Date(createdOn), "MM/dd/yyyy")}</td>
            <td className="align-middle border-top text-nowrap">
                <Button variant="link" size="sm" onClick={continueUpdateProduct}>
                    <Icon.Edit size={18}/>
                </Button>
                <Button variant="link" size="sm" onClick={continueDelete}>
                    <Icon.Trash size={18}/>
                </Button>
            </td>
        </tr>
    )
}