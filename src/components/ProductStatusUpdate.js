import { useContext, useState, useEffect } from "react";
import ProductContext from "../ProductContext";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ProductStatusUpdate() {
    const { products, setProducts, selProduct, setSelProduct, showUpdStatusModal, setShowUpdStatusModal } = useContext(ProductContext);
    
    const [product, setProduct] = useState({
        isActive: false,
        name: ""
    });

    useEffect(() => {
        if (selProduct !== null) {
            setProduct({
                isActive: selProduct.isActive,
                name: selProduct.name
            });
        }
    }, [selProduct]);

    async function changeStatus() {
        const { _id, isActive } = selProduct;
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/change-status`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ isActive: !isActive })
        });

        const changed = await res.json();

        if (changed) {
            setProducts(products.map(p => p._id === selProduct._id ? { ...p, isActive: !isActive } : p));
            setShowUpdStatusModal(false);
            setSelProduct(null);
        }
    }

    return (
        <Modal show={showUpdStatusModal} onHide={() => setShowUpdStatusModal(false)} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>{ product.isActive ? "Inactivate" : "Activate"} Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Click continue to { product.isActive ? "inactivate" : "activate"} <strong>{product.name}</strong>.
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-between">
                <Button variant={ product.isActive ? "danger" : "primary"} className="rounded-0" onClick={changeStatus}>
                    Continue
                </Button>
                <Button variant="secondary" className="rounded-0" onClick={() => setShowUpdStatusModal(false)}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}