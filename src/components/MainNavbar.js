import { useContext, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import UserContext from "../UserContext";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Form from 'react-bootstrap/Form';
import { ShoppingCart, LogOut, User, ShoppingBag, Heart, Linkedin, Twitter, Facebook  } from 'react-feather';

export default function MainNavbar() {
  const { user, cartItemCount } = useContext(UserContext);

  // Active categories
  const [categories, setCategories] = useState([]);

  async function getCategories() {
    const res = await fetch(`${process.env.REACT_APP_API_URL}/categories/active`);
    const data = await res.json();
    setCategories(data); 
  }

    useEffect(() => {
        getCategories();
    }, []);

  return (
    <>
      <Container>
        <div className="d-flex align-items-center justify-content-between">
          <Nav>
            <Nav.Item>
              <Nav.Link href="https://www.linkedin.com/" target="_blank" className="text-muted ps-0">
                <div className="social-media-icon bg-light border text-muted rounded-circle d-flex align-items-center justify-content-center" style={{ width: "25px", height: "25px" }}>
                  <Linkedin size={15} />
                </div>
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="https://www.twitter.com/" target="_blank" className="text-muted ps-0 ">
                <div className="social-media-icon bg-light border text-muted rounded-circle d-flex align-items-center justify-content-center" style={{ width: "25px", height: "25px" }}>
                  <Twitter size={15} />
                </div>
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="https://www.facebook.com/" target="_blank" className="text-muted ps-0 ">
                <div className="social-media-icon bg-light border text-muted rounded-circle d-flex align-items-center justify-content-center" style={{ width: "25px", height: "25px" }}>
                  <Facebook size={15} />
                </div>
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <Nav className="justify-content-end">
              {
                user.id !== null ?
                <NavDropdown title={<><User size={20}/><span className="align-middle ms-1">{user.name.charAt(0).toUpperCase() + user.name.slice(1)}</span></>} id="user-profile-nav-dropdown" className="user-profile-nav-dropdown text-muted">
                  <NavDropdown.Item href="#" hidden><User className="text-muted" size={15} /> <span className="align-middle">Manage Account</span></NavDropdown.Item>
                  <NavDropdown.Item as={Link} to="/orders">
                    <ShoppingBag className="text-muted" size={15} /> <span className="align-middle">My Orders</span>
                  </NavDropdown.Item>
                  <NavDropdown.Item hidden href="#"><Heart className="text-muted" size={15} /> <span className="align-middle">Wishlist</span></NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item as={Link} to="/logout"><LogOut className="text-muted" size={15} /> <span className="align-middle">Logout</span></NavDropdown.Item>
                </NavDropdown>
                :
                <>
                  <Nav.Item>
                    <Nav.Link as={Link} to="/login" eventKey="/login" className="text-muted"><small>Login</small></Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link as={Link} to="/register" eventKey="/register" className="text-muted"><small>Register</small></Nav.Link>
                  </Nav.Item>
                </>
              }
          </Nav>
        </div>
      </Container>
      <Navbar collapseOnSelect expand="lg" bg="light" variant="light" sticky="top">
        <Container>
        <Navbar.Brand as={Link} to="/">PrescyShop</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
            <Nav.Link as={Link} to="/all-products">All Products</Nav.Link>
              {
                categories.length > 0 ?
                  categories.map((c, i) => <Nav.Link key={i} as={Link} to={`/products/${c.slug}`}>{c.name}</Nav.Link>) : ""
              }
            </Nav>
            <Form className="d-none">
              <Form.Control
                type="search"
                placeholder="Search"
                className="me-2 rounded-0"
                aria-label="Search"
              />
            </Form>
            <Nav>
              <Nav.Link as={Link} to="/cart" className="position-relative">
                <ShoppingCart />
                <span className="position-absolute translate-middle badge rounded-pill bg-primary opacity-75">
                  { cartItemCount }
                  <span className="visually-hidden">cart added product count</span>
                </span>
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}