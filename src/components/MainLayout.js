import { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import MainNavbar from "./MainNavbar";
import Home from "../pages/Home";
import ProductsAll from "../pages/ProductsAll";
import Products from "../pages/Products";
import ProductView from "../pages/ProductView";
import Login from "../pages/Login";
import Register from "../pages/Register";
import ForgotPassword from "../pages/ForgotPassword";
import Cart from "../pages/Cart";
import Orders from "../pages/Orders";
import Logout from "../pages/Logout";
import Container from 'react-bootstrap/Container';

export default function MainLayout() {
    useEffect(()=>{
        document.querySelector("body").classList.remove("bg-light")
    },[]);

    // Return today's date and time
    const currentTime = new Date();

    // returns the year (four digits)
    const year = currentTime.getFullYear()

    return (
        <Router>
            <MainNavbar/>
            <Container>
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route path="/all-products" element={<ProductsAll />} />
                    <Route path="/products/:categorySlug" element={<Products />} />
                    <Route path="/product/:productSlug" element={<ProductView />} />
                    <Route path="/login" element={<Login />} />
                    <Route exact path="/register" element={<Register />} />
                    <Route exact path="/cart" element={<Cart />} />
                    <Route exact path="/orders" element={<Orders />} />
                    <Route exact path="/logout" element={<Logout />} />
                </Routes>
            </Container>
            <footer className="bg-dark text-center text-light p-5">
                &copy; Copyright {year} - <Link to="/" className="text-decoration-none">PrescyShop</Link> 
            </footer>
        </Router>
    )
}