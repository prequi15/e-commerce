import { useState, useEffect, useContext } from 'react';
import CategoryContext from '../CategoryContext';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

export default function CategoryAddForm() {
    const { categories, setCategories } = useContext(CategoryContext);
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");

    const [btnEnabled, setBtnEnabled] = useState(false);
    const [addCategoryErrMsg, setAddCategoryErrMsg] = useState("");

    async function addCategory(e) {
        e.preventDefault();
        if (btnEnabled === false) return;

        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ name, description })
        });

        const addedCategory = await res.json();
                
        if (typeof addedCategory.auth !== "undefined") {
            setAddCategoryErrMsg("Something went wrong! Please try again later.");
            return;
        }

        // Success
        if (typeof addedCategory.errorCode === "undefined") {
            const updatedList = [...categories.reverse(), addedCategory].reverse();
            setCategories(updatedList);
            setAddCategoryErrMsg("");
            setName("");
            setDescription("");
        }
        // Category already exists!
        else if (addedCategory.errorCode === 2) {
            setAddCategoryErrMsg(`Category name "${name}" already exists!`);
            setName("");
        }
        // Fail to add category
        else {
            setAddCategoryErrMsg("Something went wrong! Please try again later.");
        }
    }

    useEffect(() => {
        if (name.trim() !== "") {
            setBtnEnabled(true);
        }
        else {
            setBtnEnabled(false);
        }
    }, [name]);

    return (
        <Card className="shadow-sm mb-3 rounded-0">
            <Card.Body>
                <Card.Title className="border-bottom pb-3">Add New Category</Card.Title>
                <Form method="POST" noValidate onSubmit={addCategory}>
                    {
                        addCategoryErrMsg !== "" ? 
                        <Alert key="danger" variant="danger">
                            {addCategoryErrMsg}
                        </Alert> : ""
                    }
                    <Form.Group className="mb-3" controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" className="rounded-0" value={name} onChange={(e)=>setName(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="slug">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={3} className="rounded-0" value={description} onChange={(e)=>setDescription(e.target.value)} />
                    </Form.Group>
                    {
                        btnEnabled ?
                            <Button variant="primary" type="submit" className="rounded-0">
                                Submit
                            </Button> :
                            <Button variant="primary" type="submit" className="rounded-0" disabled>
                                Submit
                            </Button>
                    }
                </Form>
            </Card.Body>
        </Card>
    )
}