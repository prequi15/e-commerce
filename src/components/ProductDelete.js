import { useContext, useState, useEffect } from "react";
import ProductContext from "../ProductContext";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function ProductDelete() {
    const { products, setProducts, selProduct, setSelProduct, showDeleteModal, setShowDeleteModal } = useContext(ProductContext);

    const [productName, setProductName] = useState("");
    
    useEffect(() => {
        if (selProduct !== null) {
            setProductName(selProduct.name);
        }
    }, [selProduct])

    async function deleteCategory() {
        const { _id } = selProduct;
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            }
        });

        const deleted = await res.json();

        if (deleted) {
            setProducts(products.map(p => p._id === _id ? { ...p, isDeleted: true } : p));
            setShowDeleteModal(false);
            setSelProduct(null);
        }
    }

    return (
        <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Delete Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Are you sure you want to delete <strong>{productName}</strong>?
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-between">
                <Button variant="danger" className="rounded-0" onClick={deleteCategory}>
                    Delete
                </Button>
                <Button variant="secondary" className="rounded-0" onClick={() => setShowDeleteModal(false)}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}