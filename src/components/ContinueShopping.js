import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";

export default function ContinueShopping() {
    return (
        <div className="d-flex align-items-center justify-content-center">
            <div className="text-center">
                <p className="text-muted">There are no items in your cart.</p>
                <div className="d-grid">
                    <Button as={Link} to="/" variant="outline-primary">Continue Shopping</Button>
                </div>
            </div>
        </div>
    )
}