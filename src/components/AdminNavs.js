import { useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import * as Icon from 'react-feather';

export default function AdminNavs() {
  const { user } = useContext(UserContext);

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" className="border-bottom">
      <Container>
        <Navbar.Brand as={Link} to="/">PrescyShop</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/"><Icon.Monitor size={20}/> <span className="align-middle">Dashboard</span></Nav.Link>
            <Nav.Link as={Link} to="/users"><Icon.Users size={20}/> <span className="align-middle">Users</span></Nav.Link>
            <Nav.Link as={Link} to="/categories"><Icon.Layers size={20}/> <span className="align-middle">Categories</span></Nav.Link>
            <Nav.Link as={Link} to="/products"><Icon.Grid size={20}/> <span className="align-middle">Products</span></Nav.Link>
            <Nav.Link as={Link} to="/orders"><Icon.ShoppingCart size={20}/> <span className="align-middle">Orders</span></Nav.Link>
          </Nav>
          <Nav>
          <Nav.Link as={Link} to="/administrators"><Icon.Settings size={20}/> <span className="align-middle">Administrators</span></Nav.Link>
            <NavDropdown title={<><Icon.User size={20}/><span className="align-middle ms-1">{user.name.charAt(0).toUpperCase() + user.name.slice(1)}</span></>} id="profile-nav-dropdown">
                <NavDropdown.Item href="#" hidden>Change Password</NavDropdown.Item>
                <NavDropdown.Divider hidden/>
                <NavDropdown.Item as={Link} to="/logout">
                    Logout
                </NavDropdown.Item>
                </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}