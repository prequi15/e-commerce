import { useContext } from "react";
import CategoryContext from "../CategoryContext";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function CategoryUpdateStatus() {
    const { categories, setCategories, selCategory, setSelCategory, showUpdStatusModal, setShowUpdStatusModal } = useContext(CategoryContext);

    let name = "";
    let isActive = true;

    if (selCategory !== null) {
        name = selCategory.name;
        isActive = selCategory.isActive;
    }

    async function changeStatus() {
        const { _id, isActive } = selCategory;
        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories/${_id}/change-status`, {
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ isActive: !selCategory.isActive })
        });

        const changed = await res.json();

        if (changed) {
            setCategories(categories.map(c => c._id === _id ? { ...c, isActive: !isActive } : c));
            setShowUpdStatusModal(false);
            setSelCategory(null);
        }
    }

    return (
        <Modal show={showUpdStatusModal} onHide={() => setShowUpdStatusModal(false)} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>{ isActive ? "Inactivate" : "Activate"} Category</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                Click continue to { isActive ? "inactivate" : "activate"} <strong>{name}</strong> category.
            </Modal.Body>
            <Modal.Footer className="d-flex justify-content-between">
                <Button variant={ isActive ? "danger" : "primary"} className="rounded-0" onClick={changeStatus}>
                    Continue
                </Button>
                <Button variant="secondary" className="rounded-0" onClick={() => setShowUpdStatusModal(false)}>
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}