import OrderProductItem from "./OrderProductItem";
import Card from "react-bootstrap/Card";
import Accordion from 'react-bootstrap/Accordion';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import { format } from "date-fns";

export default function OrderItem({ item }) {
    const { orderIdNumber, purchasedOn, totalAmount, products } = item;
    let totalQty = 0;
    products.forEach(p => totalQty += p.quantity);
    const productList = products.reverse();
    
    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    return (
        <>
            <Card className="rounded-0 shadow border-0 mb-3">
                <Card.Body>
                    <Card.Title>Order ID: <span className="text-muted">#{orderIdNumber}</span></Card.Title>
                    <Row>
                        <Col>
                            <Card className="text-center rounded-0">
                                <Card.Header className="border-bottom-0 fw-bold">Order Date:</Card.Header>
                                <Card.Body>
                                    {format(new Date(purchasedOn), "MMM dd, yyyy hh:mm a")}
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="text-center rounded-0">
                                <Card.Header className="border-bottom-0 fw-bold">Order Total:</Card.Header>
                                <Card.Body>
                                    {numberFormat.format(totalAmount)}
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card className="text-center rounded-0">
                                <Card.Header className="border-bottom-0 fw-bold">Items:</Card.Header>
                                <Card.Body>
                                    {totalQty}
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                    <Accordion className="mt-3">
                        <Accordion.Item className="rounded-0" eventKey="1">
                            <Accordion.Header><strong>Product Summary</strong></Accordion.Header>
                            <Accordion.Body>
                                <Table striped className="table-borderless mb-0">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Sub Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { 
                                            productList.map((p, i) => 
                                                <OrderProductItem key={`order-product-item-${i}`} productId={p.productId} price={p.price} quantity={p.quantity} subtotal={p.subtotal} />)
                                        }
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td className="border-top fw-bold" colSpan={4}>Total Amount</td>
                                            <td className="border-top">{numberFormat.format(totalAmount)}</td>
                                        </tr>
                                    </tfoot>
                                </Table>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Card.Body>
            </Card>
        </>
    )
}