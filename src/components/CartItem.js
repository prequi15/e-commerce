import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import { Trash } from 'react-feather';

export default function CartItem({ product, cartItem, removeItem }) {
    const { photoUrl, name, description, slug } = product;
    const { price, quantity, productId, subtotal  } = cartItem;

    const [qty, setQty] = useState(quantity);

    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    useEffect(() => {
        
    }, [qty]);

    return (
        <>
            <Card className="rounded-0 shadow-sm mb-3">
                <Card.Body>
                    <Trash size={17} className="text-muted float-end" style={{ cursor: "pointer" }} onClick={() => removeItem(productId, quantity, subtotal)} />
                    <div className="d-flex align-items-top gap-3">
                        <div style={{ width: "120px"}}>
                            <Link to={`/product/${slug}`}>
                                <img className="img-fluid" src={photoUrl === "" ? "/no-product-image.jpg" : photoUrl} alt={name} />                            
                            </Link>
                        </div>
                        <div className="w-75">
                            <div>
                                <h6 className="mb-0">{name}</h6>
                                <p className="text-nowrap text-ellipsis mb-1 text-muted overflow-hidden">{description}</p>
                                <div className="text-primary fw-bold mb-2">{numberFormat.format(price)}</div>
                                <Form.Group className="d-flex gap-2" controlId={`cart-item-${productId}`}>
                                    <Form.Label className="mb-0 text-muted">Qty</Form.Label>
                                    <Form.Control disabled style={{ width: "80px" }} className="rounded-0 ps-4 text-center" size="sm" type="number" value={qty} onChange={(e) => setQty(e.target.value)} />
                                </Form.Group>
                            </div>
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </>
    )
}