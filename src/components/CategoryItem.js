import { useContext } from "react";
import { Link } from "react-router-dom";
import CategoryContext from "../CategoryContext";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import * as Icon from "react-feather";
import { format } from "date-fns";

export default function CategoryItem({ item }) {
    const { _id, name, description, isActive, createdOn } = item;
    const { setSelCategory, setShowUpdStatusModal, setShowUpdateModal, setShowDeleteModal } = useContext(CategoryContext);

    function continueUpdStatus() {
        setSelCategory(item);
        setShowUpdStatusModal(true);
    }

    function continueUpdInfo() {
        setSelCategory(item);
        setShowUpdateModal(true);
    }

    function continueDelete() {
        setSelCategory(item);
        setShowDeleteModal(true);
    }

    return (
        <tr>
            <td className="align-middle border-top">{ name }</td>
            <td className="align-middle border-top">{ description !== "" ? description : <small><em className="text-muted">No description</em></small>}</td>
            <td className="align-middle border-top">
                <Form.Check 
                    type="switch"
                    id={`custom-switch-${item._id}`}
                    label=""
                    onChange={continueUpdStatus}
                    checked={isActive}
                />
            </td>
            <td className="align-middle border-top">{format(new Date(createdOn), 'MM/dd/yyyy')}</td>
            <td className="align-middle border-top text-nowrap">
                <Button as={Link} to={`/categories/${_id}`} variant="link" size="sm" onClick={continueUpdInfo}>
                    <Icon.Edit size={18}/>
                </Button>
                <Button variant="link" size="sm" onClick={continueDelete}>
                    <Icon.Trash size={18}/>
                </Button>
            </td>
        </tr>
    )
}