import { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AdminNavs from "./AdminNavs";
import Dashboard from "../pages/admin/Dashboard";
import Customers from "../pages/admin/Customers";
import Categories from "../pages/admin/Categories";
import Products from "../pages/admin/Products";
import Orders from "../pages/admin/Orders";
import Users from "../pages/admin/Users";
import Login from "../pages/Login";
import Logout from "../pages/Logout";
import Container from "react-bootstrap/Container";

export default function AdminLayout() {
    useEffect(()=>{
        document.querySelector("body").classList.add("bg-light")
    },[])
    return (
        <Router>
            <AdminNavs/>
            <Container className="mt-5 pt-4">
                <Routes>
                    <Route exact path="/" element={<Dashboard />} />
                    <Route exact path="/categories" element={<Categories />} />
                    <Route exact path="/categories/:categoryId" element={<Categories />} />
                    <Route exact path="/users" element={<Customers />} />
                    <Route exact path="/products" element={<Products />} />
                    <Route exact path="/products/:productId" element={<Products />} />
                    <Route exact path="/orders" element={<Orders />} />
                    <Route exact path="/administrators" element={<Users />} />
                    <Route exact path="/login" element={<Login />} />
                    <Route exact path="/logout" element={<Logout />} />
                </Routes>
            </Container>
        </Router>
    )
}