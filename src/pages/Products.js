import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import ProductGrid from "../components/ProductGrid";

export default function Products() {
    const { categorySlug } = useParams();

    // Active categories
    const [category, setCategory] = useState({ name: "" });
    const [products, setProducts] = useState([]);

    async function getDataObj() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/${categorySlug}/category`);
        const data = await res.json();
        if (typeof data.category !== "undefined") {
            setCategory(data.category);
            setProducts(data.products);
        }
    }

    useEffect(() => {
        getDataObj();
    }, [categorySlug]);

    return (
        <>
            <h2 className="mt-3 mb-3">{category.name}</h2>
            {
                category.description !== "" ?
                    <p className="text-muted">{category.description}</p> : ""
            }
            <div className="min-height-70vh">
                <ProductGrid products={products} />
            </div>
        </>
    )
}