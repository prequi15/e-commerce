import { useContext, useState, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import OrderItem from "../components/OrderItem";
import ContinueShopping from "../components/ContinueShopping";

export default function Order() {
    const { user } = useContext(UserContext);
    const [orders, setOrders] = useState([]);

    async function getOrders() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/orders/myOrders`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        const orderList = await res.json();
        if (orderList.length > 0) {
            setOrders(orderList.reverse());
        }
    }

    useEffect(() => {
        getOrders();
    }, []);

    return (
        <>
            {
                user.id === null ? <Navigate to="/login?return_url=orders" /> :
                <>
                    <h2 className="mt-3 mb-3">My Orders</h2>
                    <div className="min-height-70vh">
                        {
                            orders.length > 0 ?
                                orders.map(o => <OrderItem key={o._id} item={o} />)
                                : <ContinueShopping />
                        }
                    </div>
                </>
            }
        </>
    )
}