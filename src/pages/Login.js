import { useState, useEffect, useContext } from 'react';
import { Link, Navigate, useNavigate, useSearchParams } from "react-router-dom";
import UserContext from "../UserContext";
import validateEmail from "../utils/validateEmail";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

export default function Login() {
    const { user, setUser, setCartItemCount } = useContext(UserContext);

    const [searchParams, setSearchParams] = useSearchParams();
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [emailValidated, setEmailValidated] = useState(null);
    const [password, setPassword] = useState("");

    const [btnEnabled, setBtnEnabled] = useState(false);
    const [verified, setVerified] = useState(null);

    const login = async (e) => {
        e.preventDefault();
        setVerified(null);
        setBtnEnabled(false);

        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({ email, password })
        });

        const data = await res.json();
        if(typeof data.access !== "undefined"){
            localStorage.setItem("token", data.access);
            getUserDetails(data.access);
            setEmail("");
        }
        else {
            // fail
            setVerified(false);
            e.stopPropagation();
        }

        setPassword("");
    }

    const getUserDetails = async (token) => {
		const res = await fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		});
        const data = await res.json();
        const { _id, isAdmin, firstName, lastName } = data.user;
        //This will be set to the user state.
        setUser({
            id: _id,
            isAdmin,
            name: `${firstName} ${lastName}`
        });

        setCartItemCount(data.cartItemCount);

        if (isAdmin) {
            navigate("/");
            return;
        }
        
        if (searchParams.get("return_url") !== null && searchParams.get("return_url") !== "") {
            navigate("/" + searchParams.get("return_url"));
        }
        else {
            navigate("/");
        }
	}

    useEffect(()=> {
        let isValid = true;
        if (email === "" || password === "") {
            isValid = false;
        }
        
        if (email !== "" && !validateEmail(email)) { 
            isValid = false; 
            setEmailValidated(false);
        }
        else {
            setEmailValidated(null);
        }

        if (isValid) {
            setBtnEnabled(true);
        }
        else {
            setBtnEnabled(false);
        }
    }, [email, password]);

    useEffect(() => {
        if (searchParams.get("email") !== null && searchParams.get("email") !== "") {
            setEmail(searchParams.get("email"));
        }
    }, []);

    return (
        <>
            {
                user.id !== null ? 
                <Navigate to="/" /> :
                <>
                {
                    searchParams.get("return_url") !== null && searchParams.get("return_url") !== "" ?
                        <div className="position-relative">
                            <div className="position-absolute w-100">
                                <div className="d-flex justify-content-center mt-4">
                                    <Alert className="rounded-0" variant="warning">
                                        Please login to continue!
                                    </Alert>
                                </div>
                            </div>
                        </div> : ""
                }
                <div className="d-flex align-items-center justify-content-center w-100 min-height-70vh">                    
                    <Row className="w-100">
                        <Col md={6}>
                            <Row>
                                <Col md={{span: 8, offset: 3}}>
                                    <h2 className="text-center mb-3">Login</h2>
                                    {
                                        verified === false ? 
                                        <Alert key="danger" variant="danger" className="text-center rounded-0">
                                            Incorrect email or password!
                                        </Alert> : ""
                                    }
                                    <Form noValidate onSubmit={login} method="POST">
                                        <Form.Group className="mb-3" controlId="email">
                                            <Form.Control type="email" placeholder="Email" className={`rounded-0 ${emailValidated === false ? "border-danger" : ""}`} value={email} onChange={e => setEmail(e.target.value)} required/>
                                            <span className="text-danger">{ emailValidated === false ? "Email is invalid." : "" }</span>
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="password">
                                            <Form.Control type="password" className="rounded-0" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
                                        </Form.Group>
                                        <div className="d-flex justify-content-between d-none">
                                            <Form.Group className="mb-3" controlId="formBasicCheckbox">
                                                <Form.Check type="checkbox" label="Remember me" />
                                            </Form.Group>
                                            <Link className="text-decoration-none" to="/forgot-password">Forgot password?</Link>
                                        </div>
                                        <div className="d-grid">
                                            {
                                                btnEnabled ?
                                                <Button variant="primary" className="rounded-0" type="submit">
                                                    Login
                                                </Button> :
                                                <Button variant="primary" className="rounded-0" type="submit" disabled>
                                                    Login
                                                </Button>
                                            }
                                        </div>
                                    </Form>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={6} className="vl-left d-none d-md-block">
                            <Row className="h-100">
                                <Col md={{span: 8, offset: 1}} className="position-relative">
                                    <h2 className="text-center mt-5 mt-md-0 mb-3">New Customer</h2>
                                    <h5>Register Now!</h5>
                                    <p>Create your account and enjoy exclusive online benefits from us!</p>
                                    <div className="d-grid position-absolute bottom-0 w-100">
                                        <Button as={Link} to="/register" variant="primary" className="rounded-0" type="button">Register</Button>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
                </>
            }
        </>
    );
}