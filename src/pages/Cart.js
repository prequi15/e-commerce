import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from "../UserContext";
import ContinueShopping from '../components/ContinueShopping';
import CartItem from '../components/CartItem';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';

export default function Cart() {
    const navigate = useNavigate();
    const { user, cartItemCount, setCartItemCount } = useContext(UserContext);
    const [cart, setCart] = useState({
        cart: null,
        products: []
    });

    async function getCart() {
        // set cart
        const resCart = await fetch(`${process.env.REACT_APP_API_URL}/orders/inCartOrder`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`
            }
          });
        const cartData = await resCart.json();
        
        if (typeof cartData.auth === "undefined" && cartData.cart !== null) {
            setCart({
                cart: cartData.cart,
                products: cartData.products.length > 0 ? cartData.products.reverse() : []
            });
        }
    }

    useEffect(() => {
        getCart();
    }, []);

    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    async function checkout() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/orders/${cart.cart._id}/checkout`, {
            method: "PUT",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "application/json"
            }
          });
        const success = await res.json();
        if (success) {
            setCartItemCount(0);
            navigate("/orders");
        }
    }

    async function removeItemFromCart(productId, quantity, subtotal) {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/orders/${productId}/removeFromCart`, {
            method: "PUT",
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "application/json"
            }
          });
        const success = await res.json();
        if (success) {
            setCartItemCount(cartItemCount - quantity);
            const updatedCartItems = cart.products.filter(p => p._id !== productId);
            setCart({
                cart: {...cart.cart, totalAmount: cart.cart.totalAmount - subtotal, cartItemCount: cart.cart.cartItemCount - quantity},
                products: updatedCartItems.length > 0 ? updatedCartItems : []
            });
        }
    }

    return (
        <>
            <h2 className="mt-3 mb-3">Cart</h2>
            <div className="min-height-60vh">
            {
                user.id === null ? <Navigate to="/login?return_url=cart" /> :
                <>
                    {
                        cart.products.length > 0 ?
                        <Row>
                            <Col md={7} lg={8}>
                                { cart.products.reverse().map(p => <CartItem key={p._id} product={p} cartItem={cart.cart.products.find(c => c.productId === p._id)} removeItem={removeItemFromCart} />) }
                            </Col>
                            <Col md={5} lg={4}>
                                <Card className="rounded-0 shadow-sm">
                                    <Card.Header className="border-bottom-0">Order Summary</Card.Header>
                                    <Card.Body>
                                        <Table className="table-borderless">
                                            <thead>
                                                <tr>
                                                    <th>Item</th>
                                                    <th className="text-end text-muted border-bottom-0">Sub Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    cart.products.map((p, i) => 
                                                        <tr key={`order-summary-${i}`}>
                                                            <td className="border-top">{p.name}</td>
                                                            <td className="text-end align-middle text-muted border-top">{numberFormat.format(cart.cart.products.find(c => c.productId === p._id).subtotal)}</td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td className="border-top border-bottom-0 fw-bold">Total Amount</td>
                                                    <td className="border-top border-bottom-0 text-primary text-end fs-5 fw-bold">{numberFormat.format(cart.cart.totalAmount)}</td>
                                                </tr>
                                            </tfoot>
                                        </Table>
                                        <div className="d-grid">
                                            <Button className="rounded-0 text-uppercase" variant="primary" onClick={checkout}>Checkout</Button>
                                        </div>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                        :
                        <ContinueShopping />
                    }                    
                </>
            }
            
            </div>
        </>
    )
}