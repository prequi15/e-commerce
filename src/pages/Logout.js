import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Logout() {
	// localStorage.clear();

	// Consume the UserContext object and destructure it to access the 
	const {setUser, setCartItemCount, unsetUser} = useContext(UserContext);

	//Clear the localStorage of the user's information
	unsetUser();
	// console.log(user);

	useEffect(() => {
		//Set the user state back to it's original value.
		setUser({
			id: null,
			isAdmin: null
		});
		setCartItemCount(0);
	})

	return (
		//Redirect back to the login
		<Navigate to="/" />
	)
}