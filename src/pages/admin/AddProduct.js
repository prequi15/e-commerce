import { useState, useEffect, useContext } from "react";
import ProductContext from "../../ProductContext";
import Modal from "react-bootstrap/Modal";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Alert from 'react-bootstrap/Alert';
import * as Icon from "react-feather";

export default function AddProduct() {
    const { products, setProducts, categories, showAddForm, setShowAddForm } = useContext(ProductContext);

    const [name, setName] = useState("");
    const [categoryId, setCategoryId] = useState("");
    const [photoUrl, setPhotoUrl] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [discount, setDiscount] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [isActive, setIsActive] = useState(true);
    const [btnEnabled, setBtnEnabled] = useState(false);
    const [showErr, setShowErr] = useState(false);

    useEffect(() => {
        if (name.trim() !== "" && categoryId !== "" && (price !== "" && price > 0) && (discount !== "" && discount >= 0)  && (stocks !== "" && stocks >= 0)) {
            setBtnEnabled(true);
        }
        else {
            setBtnEnabled(false);
        }

        if (showAddForm === false) {
            clearFields()
        }
    }, [name, categoryId, price, discount, stocks, photoUrl, showAddForm]);

    async function addProduct(e) {
        e.preventDefault();
        if (btnEnabled === false) return;

        const res = await fetch(`${process.env.REACT_APP_API_URL}/products`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ name, categoryId, photoUrl, description, price, discount, stocks, isActive })
        });

        const data = await res.json();
                
        if (typeof data.auth !== "undefined") {
            setShowErr(true);
            return;
        }

        // Success
        if (data) {
            setProducts([...products.reverse(), data].reverse());
            setShowAddForm(false);
            clearFields();
        }
        else {
            setShowErr(true);
        }
    }
    
    function clearFields() {
        setName("");
        setCategoryId("");
        setPhotoUrl("");
        setDescription("");
        setPrice(0);
        setDiscount(0);
        setStocks(0);
        setIsActive(true);
        setShowErr(false);
    }

    return (
        <>
            <Modal fullscreen show={showAddForm} onHide={() => setShowAddForm(false)} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add New Product</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        {
                            showErr ?
                                <Alert key="danger" variant="danger">
                                    Unable to add product! Please try again later.
                                </Alert>
                                : ""
                        }
                        <Form noValidate method="POST" onSubmit={addProduct}>
                            <Row>
                                <Col lg={4} xl={3}>
                                    <Form.Label className="text-center d-block">Product Photo</Form.Label>
                                    <div className="mx-auto border mb-3 position-relative" style={{ width: "270px", height: "300px" }}>
                                        <Button variant="link" size="sm" className="d-none position-absolute rounded-0 top-0 end-0 mt-1 me-1">
                                            <Icon.Edit2 />
                                        </Button>
                                        <div className="w-100 h-100 d-flex align-items-center overflow-hidden">
                                            {
                                                photoUrl.trim() !== "" ?
                                                    <img className="img-fluid" src={photoUrl.trim()} alt="Product Photo"/> : ""
                                            }
                                        </div>
                                    </div>
                                    <div className="d-none justify-content-center gap-3 mb-3">
                                        <div className="border position-relative" style={{ width: "80px", height: "80px" }}>
                                            <Button variant="link" size="sm" className="position-absolute rounded-0 top-0 end-0">
                                                <Icon.Edit2 size={20}/>
                                            </Button>
                                            <div className="w-100 h-100"></div>
                                        </div>
                                        <div className="border position-relative" style={{ width: "80px", height: "80px" }}>
                                            <Button variant="link" size="sm" className="position-absolute rounded-0 top-0 end-0">
                                                <Icon.Edit2 size={20} />
                                            </Button>
                                            <div className="w-100 h-100"></div>
                                        </div>
                                        <div className="border position-relative" style={{ width: "80px", height: "80px" }}>
                                            <Button variant="link" size="sm" className="position-absolute rounded-0 top-0 end-0">
                                                <Icon.Edit2 size={20} />
                                            </Button>
                                            <div className="w-100 h-100"></div>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={8} xl={9}>
                                    <Row>
                                        <Form.Group as={Col} className="mb-3" controlId="product_name">
                                            <Form.Label>Product Name</Form.Label>
                                            <Form.Control type="text" className="rounded-0" value={name} onChange={(e) => setName(e.target.value)} />
                                        </Form.Group>
                                        <Form.Group as={Col} className="mb-3" controlId="category">
                                            <Form.Label>Category</Form.Label>
                                            <Form.Select className="rounded-0" value={categoryId} onChange={(e) => setCategoryId(e.target.value)}>
                                                <option value="">Select Category</option>
                                                {
                                                    categories.length > 0 ?
                                                        categories.map((c, i) => <option key={i} value={c._id}>{c.name} {c.isActive ? "(Active)" : "(Inactive)"}</option>) : ""
                                                }
                                            </Form.Select>
                                        </Form.Group>
                                    </Row>
                                    <Form.Group className="mb-3" controlId="photoUrl">
                                        <Form.Label>Image URL</Form.Label>
                                        <Form.Control type="text" className="rounded-0" value={photoUrl} onChange={(e) => setPhotoUrl(e.target.value)} />
                                        <Form.Text className="text-muted">
                                            Make sure to input a valid image URL
                                        </Form.Text>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="description">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control as="textarea" rows={4} className="rounded-0" value={description} onChange={(e) => setDescription(e.target.value)} />
                                    </Form.Group>
                                    <Row>
                                        <Form.Group as={Col} className="mb-3" controlId="price">
                                            <Form.Label>Price</Form.Label>
                                            <Form.Control type="number" className="rounded-0" value={price} onChange={(e) => setPrice(e.target.value)} />
                                        </Form.Group>
                                        <Form.Group as={Col} className="mb-3" controlId="dicscount">
                                            <Form.Label>Discount</Form.Label>
                                            <InputGroup>
                                                <Form.Control type="number" className="rounded-0" aria-describedby="discount-addon" value={discount} onChange={(e) => setDiscount(e.target.value)} />
                                                <InputGroup.Text id="discount-addon" className="rounded-0">%</InputGroup.Text>
                                            </InputGroup>
                                        </Form.Group>
                                        <Form.Group as={Col} className="mb-3" controlId="stock">
                                            <Form.Label>Stocks</Form.Label>
                                            <Form.Control type="number" className="rounded-0" value={stocks} onChange={(e) => setStocks(e.target.value)} />
                                        </Form.Group>
                                    </Row>
                                    <Form.Check className="mb-3"
                                        type="checkbox"
                                        id="product-is-active-checkbox"
                                        label="Active"
                                        onChange={() => setIsActive(!isActive)}
                                        checked={isActive}
                                    />
                                    {
                                        btnEnabled ? 
                                            <Button variant="primary" type="submit" className="rounded-0" size="lg">Submit</Button> :
                                            <Button variant="primary" type="submit" className="rounded-0" size="lg" disabled>Submit</Button>
                                    }
                                </Col>
                            </Row>
                        </Form>
                    </Container>
                </Modal.Body>
            </Modal>
        </>
    )
}