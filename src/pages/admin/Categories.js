import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { CategoryProvider } from "../../CategoryContext";
import AdminPageTitle from "../../components/AdminPageTitle";
import CategoryAddForm from "../../components/CategoryAddForm";
import CategoryItem from "../../components/CategoryItem";
import UpdateCategory from "./UpdateCategory";
import CategoryUpdateStatus from "../../components/CategoryUpdateStatus";
import CategoryDelete from "../../components/CategoryDelete";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';

export default function Categories() {
    const [categories, setCategories] = useState([]);
    const [selCategory, setSelCategory] = useState(null);
    const [showUpdStatusModal, setShowUpdStatusModal] = useState(false);
    const [showUpdateModal, setShowUpdateModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const { categoryId } = useParams();

    async function getCategories() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories`);
        const data = await res.json();
        
        setCategories(data.reverse());
        
        if (typeof categoryId !== "undefined") {
            setSelCategory(data.find(c => c._id === categoryId));
            setShowUpdateModal(true);
        }
    }

    useEffect(() => {
        getCategories();
    }, []);

    return (
        <CategoryProvider value={{ categories, setCategories, selCategory, setSelCategory, showUpdStatusModal, setShowUpdStatusModal, showUpdateModal, setShowUpdateModal, showDeleteModal, setShowDeleteModal }}>
            <AdminPageTitle title="Categories" />
            <Row>
                <Col lg={4} xl={3}>
                    <CategoryAddForm />
                </Col>
                <Col lg={8} xl={9}>
                    <Card className="shadow-sm rounded-0">
                        <Card.Body>
                            <Table responsive className="table-borderless">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Active</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        categories.length > 0 ?
                                            categories.filter(c => c.isDeleted === false).map((c, i) => <CategoryItem key={i} item={c} />) :
                                            <tr>
                                                <td className="text-center text-muted border-top" colSpan={4}>No categories yet</td>
                                            </tr>
                                    }
                                </tbody>
                            </Table>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
                                
            <CategoryUpdateStatus />
            <UpdateCategory />
            <CategoryDelete />
        </CategoryProvider>
    )
}