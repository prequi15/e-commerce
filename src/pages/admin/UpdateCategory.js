import { useContext, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import CategoryContext from "../../CategoryContext";
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';

export default function UpdateCategory() {
    const { categories, setCategories, selCategory, setSelCategory, showUpdateModal, setShowUpdateModal } = useContext(CategoryContext);
    
    const navigate = useNavigate();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");

    const [btnEnabled, setBtnEnabled] = useState(false);
    const [errMsg, setErrMsg] = useState("");

    useEffect(() => {
        if (selCategory !== null) {
            setName(selCategory.name);
            setDescription(selCategory.description);
        }
    }, [selCategory]);

    async function updateCategory(e) {
        e.preventDefault();
        const { _id } = selCategory;
        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories/${_id}`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ name, description })
        });

        const updatedCategory = await res.json();
        
        if (typeof updatedCategory.auth !== "undefined") {
            setErrMsg("Something went wrong! Please try again later.");
            return;
        }

        // Success
        if (typeof updatedCategory.errorCode === "undefined") {
            setCategories(categories.map(c => c._id === _id ? { ...c, name, description } : c));
            setSelCategory(null);
            setShowUpdateModal(false);
            setErrMsg("");
            setName("");
            setDescription("");
        }
        // Category already exists!
        else if (updatedCategory.errorCode === 2) {
            setErrMsg(`Category name "${name}" already exists!`);
            setName("");
        }
        // Fail to add category
        else {
            setErrMsg("Something went wrong! Please try again later.");
        }
    }

    function closeEditModal() {
        navigate("/categories");
        setShowUpdateModal(false);
    }

    useEffect(() => {
        if (name.trim() !== "") {
            setBtnEnabled(true);
        }
        else {
            setBtnEnabled(false);
        }
    }, [name]);

    return (
        
        
            <Modal size="sm" show={showUpdateModal} onHide={closeEditModal} backdrop="static"
            keyboard={false}>
                <Form method="POST" noValidate onSubmit={updateCategory}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update Category</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            {
                                errMsg !== "" ? 
                                <Alert key="danger" variant="danger">
                                    {errMsg}
                                </Alert> : ""
                            }
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" className="rounded-0" value={name} onChange={(e)=>setName(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="slug">
                                <Form.Label>Description</Form.Label>
                                <Form.Control as="textarea" rows={3} className="rounded-0" value={description} onChange={(e)=>setDescription(e.target.value)} />
                            </Form.Group>
                    </Modal.Body>
                    <Modal.Footer className="d-flex justify-content-between">
                        {
                            btnEnabled ?
                            <Button variant="primary" type="submit" className="rounded-0">
                                Submit
                            </Button> :
                            <Button variant="primary" type="submit" className="rounded-0" disabled>
                                Submit
                            </Button>
                        }
                        <Button variant="secondary" className="rounded-0" onClick={closeEditModal}>
                            Cancel
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
    )
}