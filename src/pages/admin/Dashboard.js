import Alert from "react-bootstrap/Alert";

export default function Dashboard() {
    return (
        <>
            <Alert variant="primary" className="text-center p-5 rounded-0">
                <h2>Welcome to <strong>PrescyShop</strong>'s Admin Dashboard</h2>
            </Alert>
        </>
    )
}