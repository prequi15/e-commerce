import { useState, useEffect } from "react";
import AdminPageTitle from "../../components/AdminPageTitle";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import { format } from "date-fns";

export default function Customers() {
    const [administrators, setAdministrators] = useState([]);

    async function getAdministrators() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/administrators`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        const data = await res.json();
        if (data.length > 0) {
            setAdministrators(data.reverse());
        }
    }

    useEffect(() => {
        getAdministrators();
    }, []);

    return (
        <>
            <AdminPageTitle 
                title="Administrators" 
            />
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Table responsive className="table-borderless">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registered Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                administrators.length > 0 ?
                                administrators.map((c, i) => 
                                        <tr key={i}>
                                            <td className="border-top">{`${c.firstName} ${c.lastName}`}</td>
                                            <td className="border-top">{c.email}</td>
                                            <td className="border-top">{format(new Date(c.createdOn), 'MMM dd, yyyy hh:mm a')}</td>                                            
                                        </tr>
                                    ) : 
                                    <tr>
                                        <td className="text-center text-muted border-top" colSpan={4}>No administrators yet</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </>
    )
}