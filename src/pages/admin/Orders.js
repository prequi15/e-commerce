import { useState, useEffect } from "react";
import AdminPageTitle from "../../components/AdminPageTitle";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Badge from "react-bootstrap/Badge";
import { format } from "date-fns";

export default function Orders() {
    const [orders, setOrders] = useState([]);

    async function getOrders() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        const data = await res.json();
        if (data.length > 0) {
            setOrders(data.reverse());
        }
    }

    useEffect(() => {
        getOrders();
    }, []);

    const numberFormat = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP'
    });

    return (
        <>
            <AdminPageTitle 
                title="Orders" 
            />
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Table responsive className="table-borderless">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Items</th>
                                <th>Order Total</th>
                                <th>Order Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                orders.length > 0 ?
                                orders.map((p, i) => 
                                        <tr key={i}>
                                            <td className="border-top">#{p.orderIdNumber}</td>
                                            <td className="border-top">{p.products.map(p => p.quantity).reduce((a, b) => a + b, 0)}</td>
                                            <td className="border-top">{numberFormat.format(p.totalAmount)}</td>
                                            <td className="border-top">{format(new Date(p.purchasedOn), 'MMM dd, yyyy hh:mm a')}</td>
                                            <td className="border-top">
                                                <Badge variant="primary">{p.status}</Badge>
                                            </td>
                                        </tr>
                                    ) : 
                                    <tr>
                                        <td className="text-center text-muted border-top" colSpan={5}>No orders yet</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </>
    )
}