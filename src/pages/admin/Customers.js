import { useState, useEffect } from "react";
import AdminPageTitle from "../../components/AdminPageTitle";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { format } from "date-fns";

export default function Customers() {
    const [customers, setCustomers] = useState([]);

    async function getCustomers() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/customers`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        const data = await res.json();
        if (data.length > 0) {
            setCustomers(data.reverse());
        }
    }

    useEffect(() => {
        getCustomers();
    }, []);

    const [showModal, setShowModal] = useState(false);
    const [selUser, setSelUser] = useState({
        firstName: "",
        lastName: ""
    });

    function confirmSetAsAdmin(user) {
        setSelUser(user);
        setShowModal(true);
    }

    function cancelSetAsAdmin() {
        setSelUser({
            firstName: "",
            lastName: ""
        });
        setShowModal(false);
    }

    async function setAsAdmin() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/users/${selUser._id}/setAsAdmin`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            }
        });
        const success = await res.json();
        if (success) {
            setCustomers(customers.map(c => c._id === selUser._id ? {...c, isAdmin: true} : c));
            cancelSetAsAdmin();
        }
    }

    return (
        <>
            <AdminPageTitle 
                title="Users" 
            />
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Table responsive className="table-borderless">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Registered Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                customers.length > 0 ?
                                customers.filter(c => c.isAdmin === false).map((c, i) => 
                                        <tr key={i}>
                                            <td className="border-top">{`${c.firstName} ${c.lastName}`}</td>
                                            <td className="border-top">{c.email}</td>
                                            <td className="border-top">{format(new Date(c.createdOn), 'MMM dd, yyyy hh:mm a')}</td>
                                            <td className="border-top">
                                                <Button variant="primary" size="sm" onClick={() => confirmSetAsAdmin(c)}>Make Admin</Button>
                                            </td>
                                        </tr>
                                    ) : 
                                    <tr>
                                        <td className="text-center text-muted border-top" colSpan={4}>No customers yet</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>

            <Modal show={showModal} onHide={cancelSetAsAdmin} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Make Admin</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    Are you sure you want <strong>{`${selUser.firstName} ${selUser.lastName}`}</strong> to be an admin?
                </Modal.Body>
                <Modal.Footer className="d-flex justify-content-between">
                    <Button variant="primary" className="rounded-0" onClick={setAsAdmin}>
                        Continue
                    </Button>
                    <Button variant="secondary" className="rounded-0" onClick={cancelSetAsAdmin}>
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}