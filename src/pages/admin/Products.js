import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { ProductProvider } from "../../ProductContext";
import AdminPageTitle from "../../components/AdminPageTitle";
import AddProduct from "./AddProduct";
import ProductTableItem from "../../components/ProductTableItem";
import ProductStatusUpdate from "../../components/ProductStatusUpdate";
import UpdateProduct from "./UpdateProduct";
import ProductDelete from "../../components/ProductDelete";
import Button from "react-bootstrap/esm/Button";
import Card from "react-bootstrap/esm/Card";
import Table from "react-bootstrap/esm/Table";

export default function Products() {
    const [products, setProducts] = useState([]);
    const [categories, setCategories] = useState([]);
    const [showAddForm, setShowAddForm] = useState(false);
    const [selProduct, setSelProduct] = useState(null);
    const [showUpdStatusModal, setShowUpdStatusModal] = useState(false);
    const [showUpdateForm, setShowUpdateForm] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const { productId } = useParams();

    async function getCategories() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/categories`);
        const data = await res.json();
        setCategories(data.reverse());
    }

    async function getProducts() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products`);
        const data = await res.json();
        
        setProducts(data.reverse());
        
        if (typeof productId !== "undefined") {
            setSelProduct(data.find(p => p._id === productId));
            setShowUpdateForm(true);
        }

        getCategories();
    }

    useEffect(() => {
        getProducts();
    }, []);

    return (
        <ProductProvider value={{ products, setProducts, categories, showAddForm, setShowAddForm, selProduct, setSelProduct, showUpdStatusModal, setShowUpdStatusModal, showUpdateForm, setShowUpdateForm, showDeleteModal, setShowDeleteModal }}>
            <AdminPageTitle 
                title="Products" 
                button={<Button onClick={() => setShowAddForm(true)} variant="outline-primary" className="rounded-0 text-uppercase">Add Product</Button>}
            />
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Table responsive className="table-borderless">
                        <thead>
                            <tr>
                                <th>Photo</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Stocks</th>
                                <th>Active</th>
                                <th>Date Added</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                products.length > 0 ?
                                    products.filter(c => c.isDeleted === false).map((p, i) => <ProductTableItem key={i} item={p} />) : 
                                    <tr>
                                        <td className="text-center text-muted border-top" colSpan={8}>No products yet</td>
                                    </tr>
                            }
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>

            <AddProduct />
            <ProductStatusUpdate />
            <UpdateProduct />
            <ProductDelete />
        </ProductProvider>
    )
}