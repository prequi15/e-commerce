import { useState, useEffect } from "react";
import ProductGrid from "../components/ProductGrid";

export default function Products() {

    // Active products
    const [products, setProducts] = useState([]);

    async function getProducts() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/active`);
        const data = await res.json();
        setProducts(data);
    }

    useEffect(() => {
        getProducts();
    }, []);

    return (
        <>
            <h2 className="mt-3 mb-3">All Products</h2>
            <div className="min-height-70vh">
                <ProductGrid products={products} />
            </div>
        </>
    )
}