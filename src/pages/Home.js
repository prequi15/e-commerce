import { useState, useEffect } from 'react';
import ProductGrid from '../components/ProductGrid';
import Carousel from 'react-bootstrap/Carousel';

export default function Home() {
    const carouselItems = [
        { image: "/pexels-mnz-1639729.jpg", title: "Fashion", slogan: "Complement your flawless beauty" },
        { image: "/pexels-pixabay-276583.jpg", title: "Furnitures", slogan: "The furniture in a house resembles the meat and potatoes of a dinner." },        
        { image: "/pexels-dmitry-zvolskiy-2062426.jpg", title: "Kitchen", slogan: "Utensils like never before" }
    ];


    // Active categories
    const [discountedProducts, setDiscountedProducts] = useState([]);

    async function getDicountedProducts() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/discounted`);
        const data = await res.json();
        setDiscountedProducts(data); 
    }

    useEffect(() => {
        getDicountedProducts();
    }, []);
    return (
        <>
            <div className="min-height-70vh">
                <Carousel>
                    {
                        carouselItems.map((item, i) =>
                            <Carousel.Item key={i}>
                                <img
                                className="d-block w-100"
                                src={item.image}
                                alt="Second slide"
                                />
                                <Carousel.Caption>
                                <h5>{item.title}</h5>
                                <p className="d-none d-sm-block">{item.slogan}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )
                    }
                </Carousel>
                <h2 className="mt-3 mb-3">Featured Products</h2>
                <ProductGrid products={discountedProducts} />
            </div>
        </>
    )
}