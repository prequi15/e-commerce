import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";
import MainLayout from "./components/MainLayout";
import AdminLayout from "./components/AdminLayout";
import './App.css';

export default function App() {
  //Global state
  //To store the user information and will be used for validation if a user is already logged in on the app or not.
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    name: ""
  });

  const [cartItemCount, setCartItemCount] = useState(0);

  //Function for clearing localStorage on logout
  const unsetUser = () =>{
    localStorage.clear();
  }

  // To update the User State upon page load if a user already exist.
  useEffect(() => {
    async function initUser() {
      const res = await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`
        }
      });
  
      const data = await res.json();
      //set the user states values with the user details upon successful login
      if(typeof data.user !== "undefined") {
        const { _id, isAdmin, firstName, lastName } = data.user;
        setUser({
          id: _id,
          isAdmin: isAdmin,
          name: `${firstName} ${lastName}`
        });
        setCartItemCount(data.cartItemCount);
      }
      //set back the initial state of the user
      else {
          setUser({
              id: null,
              isAdmin: null,
              name: ""
            });
      }
    }

    initUser();
  }, []);

  return (
    <>
      <UserProvider value={{user, setUser, unsetUser, cartItemCount, setCartItemCount}}>
        { user.isAdmin ? <AdminLayout/> : <MainLayout/> }
      </UserProvider>
    </>
  );
}
